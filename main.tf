terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.0.2"
    }
  }

  required_version = ">= 1.1.0"
}

provider "azurerm" {
  features {}
}
resource "azurerm_resource_group" "aks_rg" {
  name     = "my-aks-rg"
  location = "East US"
}

resource "azurerm_kubernetes_cluster" "k8s" {
  name                = "my-aks-cluster"
  location            = azurerm_resource_group.aks_rg.location
  resource_group_name = azurerm_resource_group.aks_rg.name
  dns_prefix          = "myakscluster"

  default_node_pool {
    name       = "default"
    node_count = 1
    vm_size    = "Standard_D2s_v3"
  }
  identity {
    type = "SystemAssigned"
  }

  tags = {
    environment = "dev"
  }
}
provider "kubernetes" {
  host                   = azurerm_kubernetes_cluster.k8s.kube_config.0.host
  client_certificate     = base64decode(azurerm_kubernetes_cluster.k8s.kube_config.0.client_certificate)
  client_key             = base64decode(azurerm_kubernetes_cluster.k8s.kube_config.0.client_key)
  cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.k8s.kube_config.0.cluster_ca_certificate)
}
provider "helm" {
  kubernetes{
    host                   = azurerm_kubernetes_cluster.k8s.kube_config.0.host
    client_certificate     = base64decode(azurerm_kubernetes_cluster.k8s.kube_config.0.client_certificate)
    client_key             = base64decode(azurerm_kubernetes_cluster.k8s.kube_config.0.client_key)
    cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.k8s.kube_config.0.cluster_ca_certificate)
  }
}
resource "kubernetes_namespace" "cicd"{
  metadata {
    name = "cicd"
  }
}
resource "kubernetes_namespace" "observation"{
  metadata {
    name = "observation"
  }
}
resource "kubernetes_namespace" "dev"{
  metadata {
    name = "dev"
  }
}
resource "kubernetes_namespace" "staging"{
  metadata {
    name = "staging"
  }
}
resource "kubernetes_namespace" "test"{
  metadata {
    name = "test"
  }
}

resource "helm_release" "jenkins" {

  name       = "jenkins-release"
  repository = "https://charts.jenkins.io"
  chart      = "jenkins"
  namespace  =   "cicd"
  set {
    name  = "service.type"
    value = "LoadBalancer"
  }

  set {
    name  = "protocolHttp"
    value = "true"
  }

  set {
    name  = "service.externalPort"
    value = 80
  }
}
resource "helm_release" "grafana" {
  name       = "grafana-release"
  repository = "https://grafana.github.io/helm-charts"
  chart      = "grafana"
  namespace  =   "observation"
  set {
    name  = "service.type"
    value = "LoadBalancer"
  }

  set {
    name  = "protocolHttp"
    value = "true"
  }

  set {
    name  = "service.externalPort"
    value = 80
  }

}
resource "helm_release" "prometheus" {
  name       = "prometehus-release"
  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "prometheus"
  namespace  =   "observation"
  set {
    name  = "service.type"
    value = "LoadBalancer"
  }

  set {
    name  = "protocolHttp"
    value = "true"
  }

  set {
    name  = "service.externalPort"
    value = 80
  }

}
resource "helm_release" "argocd" {
  name       = "argocd-release"
  repository = "https://argoproj.github.io/argo-helm"
  chart      = "argo-cd"
  namespace  =   "observation"
  set {
    name  = "service.type"
    value = "LoadBalancer"
  }

  set {
    name  = "protocolHttp"
    value = "true"
  }

  set {
    name  = "service.externalPort"
    value = 80
  }

}